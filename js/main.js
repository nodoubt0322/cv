(function($){
	var info = $('.info-container'),
			work = $('.work-container');

	$('svg.next').click(function(){
		info.hide();
		work.show();
	});

	$('svg.prev').click(function(){
		info.show();
		work.hide();
	});	
})(jQuery)
